﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Twilio;

namespace CheckTwilio
{
    class Program
    {
        public static readonly string no_Customer_Message = ConfigurationManager.AppSettings["NO_PATIENT_MESSAGE"];
        public static readonly string accountSID = ConfigurationManager.AppSettings["AccountSID"];
        public static readonly string authToken = ConfigurationManager.AppSettings["AuthToken"];
        public static readonly string mobileFrom = ConfigurationManager.AppSettings["MobileFrom"];
        public static readonly string countryExtn = ConfigurationManager.AppSettings["IndiaPhoneExtn"];
      //  public static readonly string failureMessage = ConfigurationManager.AppSettings["FailureMessage"];
        
        static void Main(string[] args)
        {
            List<Message> responseMsg = new List<Message>();

            responseMsg.Add(SendSMS("+919986661750", "Test frm Laptop"));
        }

        public static Twilio.Message SendSMS(string mobileNo, string message)
        {
            var twilio = new Twilio.TwilioRestClient(accountSID, authToken);
            Twilio.Message twilioResult = twilio.SendMessage(mobileFrom, mobileNo, message, "");
            return twilioResult;
        }
    }
}
